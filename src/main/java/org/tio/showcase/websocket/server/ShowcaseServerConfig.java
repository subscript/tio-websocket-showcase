/**
 * 
 */
package org.tio.showcase.websocket.server;

import org.tio.server.ServerGroupContext;
import org.tio.showcase.websocket.server.processor.ServerProcessor;
import org.tio.utils.time.Time;

/**
 * @author tanyaowu
 * @modify by huang lin
 *
 */
public abstract class ShowcaseServerConfig {
	/**
	 * 协议名字(可以随便取，主要用于开发人员辨识)
	 */
	public static final String PROTOCOL_NAME = "showcase";
	
	public static final String CHARSET = "utf-8";
	/**
	 * 监听的ip null表示监听所有，并不指定ip
	 */
	public static final String SERVER_IP = null;

	/**
	 * 监听端口
	 */
	public static final int SERVER_PORT = 9326;

	/**
	 * 心跳超时时间，单位：毫秒
	 */
	public static final int HEARTBEAT_TIMEOUT = 1000 * 600;

	/**
	 * 如果使用DefaultServerProcessor就是单节点，shiyong ServerProcessorOnPubSub就能集群了
	 */
	public static ServerProcessor processor;

	/**
	 * 给MsgUtil hold住实例，直接调用
	 */
	public static ServerGroupContext groupContext;

	/**
	 * ip数据监控统计，时间段
	 * @author tanyaowu
	 *
	 */
	public interface IpStatDuration {
		Long DURATION_1 = Time.MINUTE_1 * 5;
		Long[] IP_STAT_DURATIONS = new Long[] { DURATION_1 };
	}

}
